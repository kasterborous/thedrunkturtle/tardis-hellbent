local PART={}
PART.ID = "hellbenthandle"
PART.Name = "Hell Bent TARDIS Handle"
PART.Model = "models/doctorwho1200/hellbent/handle.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 5

PART.Sound = "doctorwho1200/hellbent/handle.wav"

TARDIS:AddPart(PART)