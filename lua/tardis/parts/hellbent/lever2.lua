local PART={}
PART.ID = "hellbentlever2"
PART.Name = "Hell Bent TARDIS Lever 2"
PART.Model = "models/doctorwho1200/hellbent/lever2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3.5

PART.SoundOff = "doctorwho1200/hellbent/leveroff.wav"
PART.SoundOn = "doctorwho1200/hellbent/leveron.wav"

TARDIS:AddPart(PART)