local PART={}
PART.ID = "hellbentwhiteswitch"
PART.Name = "Hell Bent TARDIS White Switch"
PART.Model = "models/doctorwho1200/hellbent/whiteswitch.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7
PART.Sound = "doctorwho1200/hellbent/rotaryswitch.wav"

TARDIS:AddPart(PART)